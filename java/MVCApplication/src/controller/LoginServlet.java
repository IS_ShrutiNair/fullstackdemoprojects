package controller;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.User;
import services.LoginService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String username,password;
	username = request.getParameter("username");
	password = request.getParameter("password");
	LoginService loginservice = new LoginService();
	boolean result = loginservice.authenticate(username, password);
	System.out.println(result);
	if(result) {
		User user = loginservice.getUserDetails(username);
		request.getSession().setAttribute("user", user);
		System.out.println(user.getFullname());
		response.sendRedirect("Success.jsp");
		return;
	}
	else {
		response.sendRedirect("LoginPage.jsp");
		return;

	}
	}

}
