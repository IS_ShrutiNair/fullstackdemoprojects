package services;

import dto.User;

public interface LoginServiceInterface {

	public boolean authenticate(String username,String password);
	public User getUserDetails(String username);
}
