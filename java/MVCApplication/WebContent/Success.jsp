<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="dto.User"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Success</title>
</head>
<body>
<h4>Login Successful!!</h4>
<%-- <% 
User user =(User)session.getAttribute("user");
%> --%>
<jsp:useBean id="user" class="dto.User" scope="session"></jsp:useBean>
<%-- Hello <%= user.getFullname() %> --%>

Hi <jsp:getProperty property="fullname" name="user"/>
</body>
</html>